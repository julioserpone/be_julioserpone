(function(){
'use strict';
var app=angular.module('SoyEmpleo');


//Dialogos Modales
app.controller('ModalCtrl', function($scope, $modal){
	var modalInstance = null;
	$scope.data = {};
	$scope.modalOpen = function (opts) {
		if( opts.resolve === '' || opts.resolve === null ){
			opts.resolve = 'data';

		}
		
		var obj= {},literal = opts.resolve;
		obj[literal] = function (){ return $scope.data; };

		var modalInstance = $modal.open({

			templateUrl: opts.templateUrl+(opts.noCache?'?'+Math.random():'') || null,

			template: opts.template || null,

			controller: opts.controller,

			size: opts.size || 'lg',

			resolve: obj
		});

	};
})
.directive('stopEvent', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			element.on(attr.stopEvent, function (e) {
				e.stopPropagation();
			});
		}
	};
});


})(); //modules
