<?php

namespace App\Providers;
use App\Company;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $table = "company";
        if (\Schema::hasTable($table)) {

            $main_company = Company::find(1)->toArray();
            
            \View::share('main_company', $main_company);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
