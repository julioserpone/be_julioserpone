<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use App\Eloquent\Model;

class Cities extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities';

    public $primaryKey  = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function state()
    {
        return $this->belongsTo('App\States');
    }
}
