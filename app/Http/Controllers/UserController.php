<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\States;
use App\Cities;
use App\User;

class UserController extends Controller
{
    private $form_rules = [
        'name'                  =>'required|string|max:100',
        'type_document'         =>'required',
        'document'              =>'required|numeric',
        'home_phone'            =>'required|string|max:15',
        'mobile_phone'          =>'required|string|max:15',
        'email'                 =>'required|email|unique:users,email,',
        'password'              =>'required|min:8|confirmed',
        'password_confirmation' =>'required|same:password|min:8',
        'address'               =>'required',
        'birthday'              =>'required|date',
        'state'                 =>'required',
        'city'                  =>'required',
        'gender'                =>'required',
        'vehicule'              =>'required',
        'license'               =>'required',
    ];

    private $panel = array(
                'center'=>['width'=>'12']
            );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Para seccionar la vista (Esta disenada para que pueda contener secciones laterales)
        $panel = $this->panel;

        //Array con los tipos de documentos (No existe tabla para esto). Uso traducciones para hacerlo mas dinamico
        $type_document = array(
            trans('user.type_document')=> [
                'C'=>trans('globals.type_document.identity_card'),
                'P'=>trans('globals.type_document.passport')
            ]);

        //Maestros
        $states = States::lists('name','id');
        $cities = Cities::lists('name','id');
        $grade_license = trans('globals.grade_license');

        return view('users.create', compact('panel','type_document','states','cities','grade_license'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), $this->form_rules);

        //Valida los valores del formulario, basado en las reglas definidas en el controlador
        if ($validator->fails()) {
            //dd($request);
            //Regresamos al formulario, con los errores encontrados y los valores ingresador al form
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        //Save User Data
        $user = new User();
        $user->state_id = $request->input('state');
        $user->city_id = $request->input('city');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->type_document = $request->input('type_document');
        $user->document = $request->input('document');
        $user->mobile_phone = $request->input('mobile_phone');
        $user->home_phone = $request->input('home_phone');
        $user->other_phone = $request->input('other_phone');
        $user->address = $request->input('address');
        $user->birthday_city = $request->input('birthday_city');
        $user->birthday = $request->input('birthday');
        $user->sex = $request->input('gender');
        $user->vehicule = $request->input('vehicule');
        $user->license = $request->input('license');
        $user->grade_license = $request->input('grade_license');
        $user->save();


        $state = States::find($user->state_id);
        $city = Cities::find($user->city_id);

        $data = [
                    'user' => $user,
                    'state' => $state,
                    'city' => $city
                ];

        $pdf = \PDF::loadView('pdf.users.register', $data);

        return $pdf->stream();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
