<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('state_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->enum('type_document', array_keys(trans('globals.type_document_abrev')));
            $table->string('document', 15);
            $table->string('language')->default('en');
            $table->string('mobile_phone')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('other_phone')->nullable();
            $table->string('address')->nullable();
            $table->string('birthday_city')->nullable();
            $table->date('birthday');
            $table->enum('sex', array_keys(trans('globals.person_sex_abrev')));
            $table->enum('vehicule', array_keys(trans('globals.verification')));
            $table->enum('license', array_keys(trans('globals.verification')));
            $table->enum('grade_license', array_keys(trans('globals.grade_license')));
            $table->rememberToken();

            //$table->foreign('state_id')->references('id')->on('states');
           // $table->foreign('city_id')->references('id')->on('cities');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
