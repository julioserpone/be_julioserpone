<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {

            $table->increments('id');
            $table->string('country', 100);
            $table->string('name', 100);
            $table->integer('zipcode');
            $table->timestamps();

        });

        Schema::table('users', function (Blueprint $table) {

            $table->foreign('state_id')->references('id')->on('states');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Sin esto, no es posible eliminar la tabla states, hasta que no se haya eliminado la clave foranea en users
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign('users_state_id_foreign');

        });
        Schema::drop('states');
    }
}
