<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('state_id')->unsigned()->nullable();
            $table->string('name', 100);

            $table->foreign('state_id')->references('id')->on('states');

            $table->timestamps();

        });

        Schema::table('users', function (Blueprint $table) {

            $table->foreign('city_id')->references('id')->on('cities');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Sin esto, no es posible eliminar la tabla cities, hasta que no se haya eliminado la clave foranea en users
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign('users_city_id_foreign');

        });
        Schema::drop('cities');
    }
}
