<?php

use App\States;
use App\Cities;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();
        $states = States::get();

        if ($states) {

            foreach ($states as $state) {
                $num_cities=$faker->numberBetween(2, 10);

                for ($i=0;$i<$num_cities;$i++) {
                    Cities::create([
                        'state_id'=> $state->id,
                        'name' => $faker->unique()->city,
                    ]);
                }
            }
        }
    }
}
