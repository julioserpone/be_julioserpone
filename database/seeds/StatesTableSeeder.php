<?php

use App\States;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();

        $country=$faker->unique()->country;

        for ($i=0;$i<24;$i++) {
            States::create([
                'country'=> $country,
                'name' => $faker->unique()->state,
                'zipcode' => $faker->postcode,
            ]);
        }
    }
}
