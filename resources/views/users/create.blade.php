@extends('layouts.master')
@section('title')@parent - {{ trans('globals.register') }} @stop
@section('page_class')user-create @stop
{{-- Content --}}
@section('content')
   @parent

   @section('center_content')
    <div class="row">
      <section class="wrapper" style='margin-top:10px;display:inline-block;width:100%;padding:15px 0 0 15px;'>

        <div class="col-lg-6 col-md-6 col-xs-10 col-sm-10 col-lg-offset-3 col-md-offset-3 col-xs-offset-1 col-xs-offset-1">
          <section class="box ">
            <header class="panel_header">
              <h2 class="title pull-left">{{ trans('globals.welcome_to') }} {{ trans('globals.soyempleo') }}</h2>                                
            </header>
            <div class="content-body">
              {!! Form::open(['route'=>'users.store', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form', 'novalidate' => 'novalidate']) !!}
                @include('users.partial.inputs')
                <div class='row'>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success btn-icon">
                            <i class="fa fa-save"></i> &nbsp; <span>{{ trans('globals.save') }}</span>
                        </button>
                    </div>
                </div>
              {!! Form::close() !!}
            </div>
          </section>
        </div>
      </section>
    </div>
    @include('partial.message') 

   @stop

@endsection
{{-- Footer --}}
@section('footer')
   @parent
@stop
{{-- Angular --}}
@section('before.angular') @stop
