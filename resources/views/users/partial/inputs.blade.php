    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('name', null, ['class'=>'form-control','placeholder' => trans('user.name_complete')] ) !!}
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::email('email', null, ['class'=>'form-control','placeholder' => trans('user.email')] ) !!}
        </div>
    </div>
    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::password('password', ['class'=>'form-control','placeholder' => trans('user.password')] ) !!}
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::password('password_confirmation', ['class'=>'form-control','placeholder' => trans('user.password_confirmation')] ) !!}
        </div>
    </div>
    <div class='row'><br></div>
    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::select('type_document',$type_document,null,['class'=>'form-control','placeholder' => trans('user.type_document')]) !!}
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('document', null, ['class'=>'form-control','placeholder' => trans('user.document_number')] ) !!}
        </div>
    </div>
    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::input('date','birthday', null, ['class'=>'form-control','placeholder' => trans('user.birthday')] ) !!}
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('birthday_city', null, ['class'=>'form-control','placeholder' => trans('user.birthday_city')] ) !!}
        </div>
    </div>
    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::select('state',$states,null,['class'=>'form-control']) !!}
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::select('city',$cities,null,['class'=>'form-control']) !!}
        </div>
    </div>
    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('address', null, ['class'=>'form-control','placeholder' => trans('user.address')] ) !!}
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('home_phone', null, ['class'=>'form-control','placeholder' => trans('user.home_phone')] ) !!}
        </div>
    </div>
    <div class='row'>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('mobile_phone', null, ['class'=>'form-control','placeholder' => trans('user.mobile_phone')] ) !!}
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('other_phone', null, ['class'=>'form-control','placeholder' => trans('user.other_phone')] ) !!}
        </div>
    </div>
    <div class='row'><br></div>
    <div class='row'>
        <div class="col-md-2 col-sm-2 col-xs-12">
            <p>{{ trans('user.gender') }}</p>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="radio-inline">
              <label>
                {!! Form::radio('gender', 'M') !!}
                {{ trans('globals.person_sex.male') }}
              </label>
            </div>
            <div class="radio-inline">
              <label>
                {!! Form::radio('gender', 'F') !!}
                {{ trans('globals.person_sex.female') }}
              </label>
            </div>
        </div>
    </div>
    <div class='row'><br></div>
    <div class='row'>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <p>{{ trans('user.own_transport') }}</p>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="radio-inline">
              <label>
                {!! Form::radio('vehicule', trans('globals.verification.yes')) !!}
                {{ trans('globals.verification.yes') }}
              </label>
            </div>
            <div class="radio-inline">
              <label>
                {!! Form::radio('vehicule', trans('globals.verification.no')) !!}
                {{ trans('globals.verification.no') }}
              </label>
            </div>
        </div>

    </div>
    <div class='row'><br></div>
    <div class='row'>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <p>{{ trans('user.driver_license') }}</p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="radio-inline">
              <label>
                {!! Form::radio('license', trans('globals.verification.yes')) !!}
                {{ trans('globals.verification.yes') }}
              </label>
            </div>
            <div class="radio-inline">
              <label>
                {!! Form::radio('license', trans('globals.verification.no')) !!}
                {{ trans('globals.verification.no') }}
              </label>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12">
            {!! Form::select('grade_license',$grade_license,null,['class'=>'form-control']) !!}
        </div>

    </div>
    <div class='row'><br></div>
    



