@extends('pdf.template')
@section('content')
<h3>{{ trans('user.user_data') }}</h3>
<table width = "100%"  rowspan = "0" border = "0">
    
    <thead>
        <tr style="background-color: #F4F4F4; color: #A1A0A0; text-align: center">
            <th colspan="2">{{ trans('user.summary_information') }}</th>
        </tr>
    </thead>

    <tbody>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.name_complete') }}</td>
            <td>{{ $user->name }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.email') }}</td>
            <td>{{ $user->email }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.type_document') }}</td>
            <td>{{ (($user->type_document=='C') ? trans('globals.type_document.identity_card') : trans('globals.type_document.passport')) }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.document_number') }}</td>
            <td>{{ $user->document }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.mobile_phone') }}</td>
            <td>{{ $user->mobile_phone }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.home_phone') }}</td>
            <td>{{ $user->home_phone }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.other_phone') }}</td>
            <td>{{ $user->other_phone }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.address') }}</td>
            <td>{{ $user->address }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.state') }}</td>
            <td>{{ $state->name }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.city') }}</td>
            <td>{{ $city->name }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.birth_date') }}</td>
            <td>{{ Carbon\Carbon::parse($user->birthday)->format('F j, Y') }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.birthday_city') }}</td>
            <td>{{ $user->birthday_city }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.sex') }}</td>
            <td>{{ $user->sex }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.own_transport') }}</td>
            <td>{{ $user->vehicule }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.driver_license') }}</td>
            <td>{{ $user->license }}</td>
        </tr>
        <tr style="text-align: left">
            <td style="text-align: right">{{ trans('user.grade_license') }}</td>
            <td>{{ $user->grade_license }}</td>
        </tr>
    </tbody>  
    
</table>

@stop