<!DOCTYPE html>
<html>
    <head>
        <title>{{ trans('globals.soyempleo').' - '.trans('globals.home') }}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
                background-image: url("img/bkg_section.jpg");
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
                color: #ffffff;
            }
            .myButton {
                -moz-box-shadow: 0px 1px 0px 0px #fff6af;
                -webkit-box-shadow: 0px 1px 0px 0px #fff6af;
                box-shadow: 0px 1px 0px 0px #fff6af;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffec64), color-stop(1, #ffab23));
                background:-moz-linear-gradient(top, #ffec64 5%, #ffab23 100%);
                background:-webkit-linear-gradient(top, #ffec64 5%, #ffab23 100%);
                background:-o-linear-gradient(top, #ffec64 5%, #ffab23 100%);
                background:-ms-linear-gradient(top, #ffec64 5%, #ffab23 100%);
                background:linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffec64', endColorstr='#ffab23',GradientType=0);
                background-color:#ffec64;
                -moz-border-radius:6px;
                -webkit-border-radius:6px;
                border-radius:6px;
                border:1px solid #ffaa22;
                display:inline-block;
                cursor:pointer;
                color:#333333;
                font-family:Arial;
                font-size:24px;
                font-weight:bold;
                padding:14px 50px;
                text-decoration:none;
                text-shadow:0px 1px 0px #ffee66;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffab23), color-stop(1, #ffec64));
                background:-moz-linear-gradient(top, #ffab23 5%, #ffec64 100%);
                background:-webkit-linear-gradient(top, #ffab23 5%, #ffec64 100%);
                background:-o-linear-gradient(top, #ffab23 5%, #ffec64 100%);
                background:-ms-linear-gradient(top, #ffab23 5%, #ffec64 100%);
                background:linear-gradient(to bottom, #ffab23 5%, #ffec64 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffab23', endColorstr='#ffec64',GradientType=0);
                background-color:#ffab23;
            }
            .myButton:active {
                position:relative;
                top:1px;
            }


        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img src="{{ asset($main_company['logo']) }}" alt="SoyEmpleo">
                <div class="title">SOY EMPLEO</div>
                <div class="row">
                    <a href="{{ route('users.create') }}" class="myButton">{{ trans('globals.sign_up') }}</a>
                </div>
            </div>
        </div>
    </body>
</html>
