<?php
//-------- For enums in Seeders --------
return [
'company_status'      => [
'active'              => 'Activa',
'inactive'            => 'Inactiva'
],
'verification'        =>[
'yes'                 => 'Si',
'no'                  => 'No'
],
'person_sex'          =>[
'female'              => 'Femenino',
'male'                => 'Masculino',
],
'person_sex_abrev'    =>[
'F'                   => 'female',
'M'                   => 'male',
],
'type_document'       =>[
'identity_card'       => 'Cedula de Identidad',
'passport'            => 'Pasaporte',
],
'type_document_abrev' =>[
'C'                   => 'Identity Card',
'P'                   => 'Passport',
],
'grade_license'       =>[
'none'                => 'Ninguna',
'first'               => 'Primera',
'second'              => 'Segunda',
'third'               => 'Tercera',
'fourth'              => 'Cuarta',
'fifth'               => 'Quinta',
],
'cancel'              => 'Cancelar',
'welcome_to'          => 'Bienvenido a',
'home'                => 'Principal',
'save'                => 'Guardar',
'register'            => 'Registro de Usuarios',
'power_by_label'      => 'Desarrollado por',
'soyempleo'           => 'SoyEmpleo.com',
'location'            => 'Ubicacion',
'sign_up'             => 'Registrarme',
];

