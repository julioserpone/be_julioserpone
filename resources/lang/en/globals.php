<?php
//-------- For enums in Seeders --------
return [
'company_status'      => [
'active'              => 'Active',
'inactive'            => 'Inactive'
],
'verification'        =>[
'yes'                 => 'Yes',
'no'                  => 'No'
],
'person_sex'          =>[
'female'              => 'Female',
'male'                => 'Male',
],
'person_sex_abrev'    =>[
'F'                   => 'female',
'M'                   => 'male',
],
'type_document'       =>[
'identity_card'       => 'Identity Card',
'passport'            => 'Passport',
],
'type_document_abrev' =>[
'C'                   => 'Identity Card',
'P'                   => 'Passport',
],
'grade_license'       =>[
'none'                => 'None',
'first'               => 'First',
'second'              => 'Second',
'third'               => 'Third',
'fourth'              => 'Fourth',
'fifth'               => 'Fifth',
],
'cancel'              => 'Cancel',
'welcome_to'          => 'Welcome to',
'home'				  => 'Home',
'save'                => 'Save',
'register'            => 'Register Users',
'power_by_label'      => 'Power By',
'soyempleo'           => 'SoyEmpleo.com',
'location'            => 'Location',
'sign_up'			  => 'Sign Up',
];
