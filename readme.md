## Laravel PHP Framework

TEST DEVELOPED BY JULIO HERNANDEZ

Pasos a seguir:
1) Instalar composer
2) Descargar el repositorio de mi branch
3) Ejecutar en la consola, dentro de la carpeta fuente, el comando composer update
4) Busca el archivo .env, y configura el acceso de la base de datos. (Debes crear solo la instancia)
5) Ejecutar por consola, php artisan migrate --seed (Esto monta las tablas y demas objetos de la base de datos, y tambien le inserta datos, como los estados y ciudades, informacion de compania, etc)
6) Esta version te permite manejar dos idiomas, ingles y espanol. Busca el archivo app.php y cambio los valores de las variales 'locale' y 'fallback_locale' (es,en)
7) Despues, si tienes problemas al ejecutar la prueba, ejecuta un composer dumpautoload.
8) Si tienes fallas con las rutas, puedes ejecutar php artisan route:cache


[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
